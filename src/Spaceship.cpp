/**
 * @file Spaceship.cpp
 * @author jmmut 
 * @date 2016-04-13.
 */

#include "Spaceship.h"

void Spaceship::boostStraight(double acceleration) {
    iterationAcceleration.z += acceleration;
}

void Spaceship::boostRight(double acceleration) {
    iterationAcceleration.x += acceleration;
}

void Spaceship::boostUp(double acceleration) {
    iterationAcceleration.y += acceleration;
}

void Spaceship::applyGravitatoryField(Vector3D force) {
    iterationAcceleration += force;
}

void Spaceship::turnUp(double acceleration) {
    iterationAngularAcceleration.x += acceleration;
}

void Spaceship::turnRight(double acceleration) {
    iterationAngularAcceleration.z -= acceleration;
}

void Spaceship::rollRight(double acceleration) {
    iterationAngularAcceleration.x += acceleration;
}

void Spaceship::advance(double deltaTime) {
    velocity = velocity + deltaTime * iterationAcceleration;
//    flyer.setPos(flyer.getPos() + velocity * deltaTime + 0.5 * iterationAcceleration * deltaTime);
    flyer.setPos(flyer.getPos() + velocity * deltaTime);
    iterationAcceleration.EsCero();
    iterationAngularAcceleration.EsCero();
}
