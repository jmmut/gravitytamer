//
// Created by jmmut on 2015-06-09.
//

#ifndef GRAVITYTAMER_GESTOR_H
#define GRAVITYTAMER_GESTOR_H


#include <stdexcept>
#include <cmath>
#include "log/log.h"
#include "vgm/Ventana.h"
#include "vgm/ShaderManager.h"
#include "graphics/drawables/Volador.h"
#include "SphereLOD.h"
#include "Spaceship.h"
#include <sstream>

struct Model {
    Model();

    vector<SphereLOD> planets;
    Spaceship spaceship;
    int detail;
    Vector3D center;
    const double G = 6.674e-20; // in units of: km^3 / (kg * s^2)
    const double EARTH_MASS = 5.9736e24;
};

class Gestor: public Ventana
{
public:
    Gestor(int width = 640, int height = 480);
    void initGL() override;
    void onStep(float) override;
    void onDisplay() override;
    void onKeyPressed(SDL_Event &e) override;
    void onPressed(const Pulsado &p) override;
    void onMouseMotion(SDL_Event &e) override;
    void onMouseButton(SDL_Event &e) override;
    
    string help();

private:
    void drawAxes();

    int t;
    float drot, dtrl, mouse_drot, mouse_dtrl;   // delta rotation, delta translation


    Volador vldr;
    bool leftClickPressed;
    bool rightClickPressed;
    Sint32 lastClickX;
    Sint32 lastClickY;

    Model model;
};

#endif //GRAVITYTAMER_GESTOR_H
