/**
 * @file main.cpp
 */

#include "utils/SignalHandler.h"
#include "Gestor.h"

using namespace std;

int main(int argc, char **argv)
{
    // this prints in cout the stack trace if there is a segfault. try dereferencing a null in an inner function
    SigsegvPrinter::activate();
    int logLevel = LOG_INFO_LEVEL;
    try {
        if (argc == 2) {
            logLevel = randomize::log::parseLogLevel(argv[1]);
        }
    } catch (invalid_argument e) {
    }
    LOG_LEVEL(logLevel);

    Gestor gestor(1300, 700);

    if (argc > 1) {
        cout << gestor.help();
    }
    gestor.mainLoop();

    cout << endl;
    return 0;
}
