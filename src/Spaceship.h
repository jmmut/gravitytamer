/**
 * @file Spaceship.h
 * @author jmmut 
 * @date 2016-04-13.
 */

#ifndef GRAVITYTAMER_SPACESHIP_H
#define GRAVITYTAMER_SPACESHIP_H

#include "graphics/drawables/Volador.h"

/**
 * moving object changing its acceleration.
 *
 * In space (without friction), any force you exert is going to accelerate you. If a spaceship has
 * boosters,
 */
class Spaceship {
public:

    Spaceship(float mass) : mass(mass) { }

    void boostStraight(double acceleration);
    void boostRight(double acceleration);
    void boostUp(double acceleration);
    void applyGravitatoryField(Vector3D force);
    void turnUp(double acceleration);
    void turnRight(double acceleration);
    void rollRight(double acceleration);

    Vector3D getPosition() { return flyer.getPos();}
    Vector3D getUp() { return flyer.getUp();}
    Vector3D getDirection() { return flyer.getDir();}
    Vector3D getVelocity() { return velocity;}
    Vector3D getAngularVelocity() { return angularVelocity;}
    const Vector3D &getIterationAcceleration() const { return iterationAcceleration; }

    void setPosition(Vector3D newPosition) {flyer.setPos(newPosition);}
    void setDirection(Vector3D newOrientation, Vector3D newUp) {
        flyer.setOrientacion(newOrientation, newUp);
    }

    void advance(double deltaTime);
private:
    Volador flyer;

    Vector3D velocity;
    Vector3D angularVelocity;

public:

private:
    Vector3D iterationAcceleration;
    Vector3D iterationAngularAcceleration;
    float mass;
};


#endif //GRAVITYTAMER_SPACESHIP_H
