//
// Created by jmmut on 2015-06-09.
//

#include "Gestor.h"

Model::Model() : spaceship(1) {
    planets.push_back(SphereLOD({6310, 0, 10}, 6300));

    detail = 2;
    spaceship.setPosition(Vector3D(1, 20, 10));
    spaceship.setDirection({0, 0, -1}, Vector3D(0, 1, 0));
    center = spaceship.getPosition();
}

Gestor::Gestor(int width, int height): Ventana(width, height) {

    SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);
    initGL();

    SDL_SetWindowTitle(window, "Gravity Tamer");
    t = 0;

    setFps(30);

    drot = 3;
    dtrl = 0.2;
    mouse_drot = 0.0625f * drot;
    mouse_dtrl = 0.08f * dtrl;
    leftClickPressed = rightClickPressed = false;

    semaforoStep.sumar();
    semaforoDisplay.sumar();
}

/** A general OpenGL initialization function.
 * Sets all of the initial parameters.
 * We call this right after our OpenGL window is created.
 */
void Gestor::initGL() {
    GLdouble aspect;
    int width = 640, height = 480;

    if (window == NULL) {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                "There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
    } else {
        SDL_GetWindowSize(window, &width, &height);
        context = SDL_GL_CreateContext(window);
        if (!context) {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to create OpenGL context: %s\n", SDL_GetError());
            SDL_Quit();
            exit(2);
        }
    }

    glViewport(0, 0, width, height);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        // This Will Clear The Background Color To Black
    glClearDepth(1.0);                // Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS);                // The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST);            // Enables Depth Testing
    glShadeModel(GL_SMOOTH);            // Enables Smooth Color Shading
    glPointSize(3);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();                // Reset The Projection Matrix

    aspect = (GLdouble)width / height;

    perspectiveGL (45, aspect, 0.001, 1000000); // in kilometers

    //glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
}

void Gestor::onStep(float dt) {
    t++;
    Vector3D toCenter = model.planets[0].getPos() - model.spaceship.getPosition();
    double r = toCenter.Modulo();
    if (r > model.planets[0].getRadius()) {
        model.spaceship.applyGravitatoryField(toCenter.Unit() * model.G * model.EARTH_MASS / (r * r));
    }
    Vector3D accel = model.spaceship.getIterationAcceleration();
    model.spaceship.advance(dt);
    vldr.setPos(model.spaceship.getPosition());
//    vldr.setOrientacion(model.spaceship.getVelocity(), model.spaceship.getUp());

    stringstream title;
    title.precision(4);
    title << "Gravity Tamer, position: " << std::fixed <<  model.spaceship.getPosition()
    << ", velocity: " << model.spaceship.getVelocity()
    << ", acceleration: " << accel;
    SDL_SetWindowTitle(window, title.str().c_str());
}

void Gestor::drawAxes() {
    glPushMatrix();
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(100, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 100, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 100);
    glEnd();

//    glBegin(GL_TRIANGLES);
//    glVertex3f(0, 0, 0);
//    glVertex3f(0, 2, 0);
//    glVertex3f(0, 2, 1);
//    glEnd();
    glPopMatrix();
}

void Gestor::onDisplay() {

    // Clear The Screen And The Depth Buffer
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glLoadIdentity();                // Reset The View
    vldr.Look();
    //	glTranslatef( 0, 0, -5.0);

    drawAxes();

    glPushMatrix();
    glBegin(GL_POLYGON);
    glColor4f(0, 0.5, 1, 0.2);
    glVertex3f(-1.0, 0.0, 0.0);
    glColor4f(0.5, 1, 0, 0.2);
    glVertex3f(1.0, 0.0, 0.0);
    glColor4f(1, 0, 0.5, 0.2);
    glVertex3f(0.0, 1.0, 0.0);
    glEnd();
    glPopMatrix();

    glPushMatrix();
    for (auto planet : model.planets) {
        // using tangent of center and distance
        planet.draw(model.detail);
//        planet.draw(10+log2(planet.getRadius() / (vldr.getPos() - planet.getPos()).Modulo()));
    }
    glPopMatrix();


    // swap buffers to display, since we're double buffered.
    SDL_GL_SwapWindow(window);
}

string Gestor::help() {
    return string("\n\t"
            "h: help:\n\t"
            "space: add one to display and step\n\t"
            "p: play/stop\n\t\t"
            "movements:\n\t\t"
            "w: move up\n\t\t"
            "a: move left\n\t\t"
            "s: move down\n\t\t"
            "d: move right\n\t\t"
            "q: quit zoom\n\t\t"
            "e: enter, add zoom\n\t"
            "rotations:\n\t\t"
            "i: rotate up\n\t\t"
            "j: rotate left\n\t\t"
            "k: rotate down\n\t\t"
            "l: rotate right\n\t\t"
            "u: roll left\n\t\t"
            "o: roll right\n\t"
            );
}

void Gestor::onPressed(const Pulsado &p) {
    //cout << "p.sym = " << p.sym << endl;	// DEPURACION
    if (p.sym == SDLK_u
        || p.sym == SDLK_o
        || p.sym == SDLK_i
        || p.sym == SDLK_k
        || p.sym == SDLK_j
        || p.sym == SDLK_l
        || p.sym == SDLK_w
        || p.sym == SDLK_s
        || p.sym == SDLK_a
        || p.sym == SDLK_d
        || p.sym == SDLK_q
        || p.sym == SDLK_e) {
        //semaforoStep.cerrar();
    semaforoDisplay.sumar();
//        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    }

    switch(p.sym) {
        case SDLK_u:
            //vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
//                vldr.rotatef(-drot, 0, 0, 1);
            model.spaceship.rollRight(-drot);
            break;
        case SDLK_o:
//            vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
//                vldr.rotatef(drot, 0, 0, 1);
            model.spaceship.rollRight(drot);
            break;
        case SDLK_i:
            //vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
//                vldr.rotatef(drot, 1, 0, 0);
            model.spaceship.turnUp(-drot);
            break;
        case SDLK_k:
            //vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
//                vldr.rotatef(-drot, 1, 0, 0);
            model.spaceship.turnUp(drot);
            break;
        case SDLK_l:
//                vldr.rotY(-drot);
            model.spaceship.turnRight(-drot);
            break;
        case SDLK_j:
//                vldr.rotY(drot);
            model.spaceship.turnRight(drot);
            break;
        case SDLK_e:
//            vldr.setPos(vldr.getPos() + vldr.getDir()*dtrl);
            model.spaceship.boostStraight(-dtrl);
            break;
        case SDLK_q:
//            vldr.setPos(vldr.getPos() - vldr.getDir()*dtrl);
            model.spaceship.boostStraight(dtrl);
            break;
        case SDLK_w:
//            vldr.setPos(vldr.getPos() + vldr.getUp()*dtrl);
            model.spaceship.boostUp(dtrl);
            break;
        case SDLK_s:
//            vldr.setPos(vldr.getPos() - vldr.getUp()*dtrl);
            model.spaceship.boostUp(-dtrl);
            break;
        case SDLK_d:
//            vldr.setPos(vldr.getPos() - vldr.getX()*dtrl);
            model.spaceship.boostRight(dtrl);
            break;
        case SDLK_a:
//            vldr.setPos(vldr.getPos() + vldr.getX()*dtrl);
            model.spaceship.boostRight(-dtrl);
            break;
        default:
            break;
    }
}
void Gestor::onKeyPressed(SDL_Event &e) {

    semaforoDisplay.sumar();
    if (e.type == SDL_KEYUP) {
        return;
    }

    switch(e.key.keysym.sym) {
        case SDLK_h:
            cout << "radius:" << model.planets[0].getRadius() << endl;
            cout << help() << endl;
            break;
        case SDLK_SPACE:
            semaforoDisplay.sumar();
            semaforoStep.sumar();
            break;
        case SDLK_p:	// play / stop
            if (semaforoDisplay.estado())
            {
                semaforoStep.cerrar();
                semaforoDisplay.cerrar();
                semaforoDisplay.sumar();
            }
            else
            {
                semaforoStep.abrir();
                semaforoDisplay.abrir();
            }
            break;
        case SDLK_z:
            model.detail += 2;
            break;
        case SDLK_x:
            model.detail -= 2;
            break;
        case SDLK_c:
            dtrl *= 10.0;
            break;
        case SDLK_v:
            dtrl /= 10.0;
            break;
        default:
            break;
    }
}

void Gestor::onMouseButton(SDL_Event &e)
{
    lastClickX = e.button.x;
    lastClickY = e.button.y;

    if (e.button.button == SDL_BUTTON_RIGHT) {
        rightClickPressed = e.button.type == SDL_MOUSEBUTTONDOWN;
    }

    if (e.button.button == SDL_BUTTON_LEFT) {
        leftClickPressed = e.button.type == SDL_MOUSEBUTTONDOWN;
    }

    if (e.button.type == SDL_MOUSEWHEEL) {
        vldr.setPos(vldr.getPos() + e.wheel.y*10*vldr.getDir()*dtrl);
    }
    semaforoDisplay.sumar();
}

void Gestor::onMouseMotion(SDL_Event &e)
{
    int w, h;
    SDL_GetWindowSize(window, &w, &h);

    if (leftClickPressed) {
        vldr.rotatef(e.motion.yrel*mouse_drot, 1, 0, 0);
        vldr.externalRotatef(-e.motion.xrel*mouse_drot, 0, 1, 0);
//        vldr.rotateAround(center, e.motion.yrel*mouse_drot, 1, 0, 0);
//        vldr.externalRotateAround(center, -e.motion.xrel*mouse_drot, 0, 1, 0);
    }

//    if (rightClickPressed) {
//        Vector3D projection_y(vldr.getDir());
//        float sense(projection_y.getY()<= 0? 1 : -1);
//        projection_y.setY(0);
//        Vector3D projection_x(vldr.getX());
//        projection_x.setY(0);
//        Vector3D translation(e.motion.yrel * mouse_dtrl * projection_y * sense + e.motion.xrel * mouse_dtrl * projection_x);
//        center += translation;
//        vldr.setPos(vldr.getPos() + translation);
//    }
    semaforoDisplay.sumar();
}



