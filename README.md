
# Gravity Tamer

This is a prototype of a space videogame made in one day.

It features 3D movement based on acceleration, which is hard to handle, but is the actual way for
spaceships to move. A clickable turning camera is available as well.

Also, a rude system of spheres with changing Level Of Detail.


To compile this project, check that you have the requirements installed, and do the following:

```
git clone git@bitbucket.org:jmmut/gravitytamer.git
cd gravitytamer
conan install
cmake -G "Unix Makefiles"
make
./bin/gravityTamer
```

## Requirements

- C++ compiler (sudo apt-get install build-essential)
- cmake (sudo apt-get install cmake)
- conan (download [here](https://www.conan.io/downloads), or do: sudo pip install conan)

